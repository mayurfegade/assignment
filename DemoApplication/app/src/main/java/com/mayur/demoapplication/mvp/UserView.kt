package com.mayur.demoapplication.mvp

/**
 * Created by Mayur on 19/08/2019 @ 17:08 IST
 */
interface UserView {

    fun fetchUserData()
}