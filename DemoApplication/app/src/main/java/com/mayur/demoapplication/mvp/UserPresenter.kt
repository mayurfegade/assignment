package com.mayur.demoapplication.mvp

/**
 * Created by Mayur on 19/08/2019 @ 17:15 IST
 */
interface UserPresenter {

    fun getUserDetails()

}
