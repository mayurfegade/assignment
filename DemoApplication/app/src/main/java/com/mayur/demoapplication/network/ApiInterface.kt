package com.mayur.demoapplication.network

import com.mayur.demoapplication.UserDetails
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

/**
 * Created by Mayur on 09/08/2019 @ 12:10 IST
 */
interface ApiInterface {

    @GET("users")
    fun getDetails(): Call<List<UserDetails>>

    companion object Factory {

        fun create(): ApiInterface {

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://api.github.com/")
                .build()

            return retrofit.create(ApiInterface::class.java)

        }

    }

}