package com.mayur.demoapplication

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import com.mayur.demoapplication.network.ApiInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.widget.Toast
import com.mayur.demoapplication.adapter.UsersRecyclerListAdapter
import com.mayur.demoapplication.mvp.UserImpl
import com.mayur.demoapplication.mvp.UserView
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Created by Mayur on 19/08/2019 @ 17:23 IST
 */

class MainActivity : AppCompatActivity(), UsersRecyclerListAdapter.itemClickListener, UserView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val userImpl = UserImpl(this)

        userImpl.getUserDetails()
    }

    override fun fetchUserData() {
        val apiCall = ApiInterface.create()
        val call = apiCall.getDetails()
        call.enqueue(object : Callback<List<UserDetails>> {
            override
            fun onResponse(call: Call<List<UserDetails>>, response: Response<List<UserDetails>>) {

                val lstUserDetails = response?.body()
                val layoutManager = GridLayoutManager(applicationContext,2)
                val usersRecyclerListAdapter = UsersRecyclerListAdapter(applicationContext, lstUserDetails, this@MainActivity)
                rv_List.layoutManager = layoutManager
                rv_List.adapter = usersRecyclerListAdapter
            }

            override
            fun onFailure(call: Call<List<UserDetails>>, t: Throwable) {
                Toast.makeText(this@MainActivity, "Fail", Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onItemClick(position: Int, userDetails: UserDetails?) {

        val intent = Intent(this@MainActivity, UserDetailsActivity::class.java)
        intent.putExtra("userDetails", userDetails)
        startActivity(intent)

    }
}
