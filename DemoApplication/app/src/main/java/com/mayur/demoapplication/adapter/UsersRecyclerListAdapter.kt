package com.mayur.demoapplication.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.mayur.demoapplication.R
import com.mayur.demoapplication.UserDetails
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

/**
 * Created by Mayur on 19/08/2019 @ 17:23 IST
 */
class UsersRecyclerListAdapter(val context: Context, private val lstUserDetails: List<UserDetails>?, val mListener: itemClickListener?) : RecyclerView.Adapter<UsersRecyclerListAdapter.MyViewHolder>() {

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        var userDetails = lstUserDetails?.get(position)
        holder.username.text = "Name : "+userDetails?.login
        holder.usertype.text = "Type : "+userDetails?.type
        holder.progress_bar.visibility = View.VISIBLE
        Picasso.with(context).load(userDetails?.avatarUrl.toString())
            .into(holder.profileImage, object : Callback {

                override fun onError() {

                }

                override fun onSuccess() {
                    holder.progress_bar.visibility = View.GONE
                }

            })

        holder.itemView.setOnClickListener(View.OnClickListener {
            mListener!!.onItemClick(position, userDetails)//item
        })

    }

    override fun getItemCount(): Int {
        return lstUserDetails?.size!!
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_user, parent, false)
        return MyViewHolder(view)
    }

    interface itemClickListener {
        fun onItemClick(position: Int, userDetails: UserDetails?)
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val progress_bar: ProgressBar = itemView.findViewById(R.id.progress_bar)
        val profileImage: ImageView = itemView.findViewById(R.id.iv_user)
        val username: TextView = itemView.findViewById(R.id.tv_username)
        val usertype: TextView = itemView.findViewById(R.id.tv_user_type)
    }
}