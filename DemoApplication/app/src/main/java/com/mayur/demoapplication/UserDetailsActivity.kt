package com.mayur.demoapplication

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.ortiz.touchview.TouchImageView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

/**
 * Created by Mayur on 19/08/2019 @ 17:23 IST
 */
class UserDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val userDetails = getIntent().getExtras().getSerializable("userDetails") as? UserDetails

        var tiv_profile = findViewById(R.id.tiv_profile) as TouchImageView

        Picasso.with(this).load(userDetails?.avatarUrl.toString())
            .into(tiv_profile, object : Callback {

                override fun onError() {
                }

                override fun onSuccess() {
                }

            })
    }
}