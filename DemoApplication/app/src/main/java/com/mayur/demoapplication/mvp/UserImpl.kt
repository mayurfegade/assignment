package com.mayur.demoapplication.mvp

/**
 * Created by Mayur on 19/08/2019 @ 17:23 IST
 */
class UserImpl internal constructor(

    private val userView: UserView
) : UserPresenter {

    override fun getUserDetails() {
        userView.fetchUserData()
    }

}
